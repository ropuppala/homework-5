#include "game.h"

/*
 *Aidan Reilly
 *Saksham Bhandari
 *Rohan Puppaa
 */

using namespace std;

// Constructs a Board
Board::Board(int size): board_size(size){
    for (int i = 0; i < size; i++){
      board.push_back(std::vector<int>(size, 0));
    }
}

// Marks a given spot on the board with given int
// if the spot is on the board, otherwise does nothing.
void Board::mark_spot(Coord spot, int mark){
  if (spot.first >= 0 && spot.first < board_size && spot.second >= 0 && spot.second < board_size){
    if (spot.first < board_size && spot.second < board_size){
      board[spot.first][spot.second] = mark;
    }
  }
}

// Constructs a Game
Game::Game(int size, int num_boards): player(1) {
    for (int i = 0; i < num_boards; i++){
      b.push_back(Board(size));
    }
}

// Updates whose turn it is
void Game::update_turn() {
  if (player == 1){
    player = 2;
  }
  else{
    player = 1;
  }
}

// Checks if a player is allowed to attack that spot
// Returns true if the spot is on the board and empty (contains a 0),
// returns false otherwise.
bool Game::is_valid_move(Coord spot, Board board) const{
  if (spot.first >= 0 && spot.first < board.get_size()
      && spot.second >= 0 && spot.second < board.get_size()){
    if (board.at_spot(spot) == 0){
      return true;
    }
    else{
      return false;
    }
  }
  else {
    return false;
  }
}

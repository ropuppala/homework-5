Aidan Reilly, Saksham Bhandari, Rohan Puppala

Above and Beyond:
- Rohan Puppala: Even though we all contrubted a fair amount to the project in our own ways, Aidan contributed significantly to the header files, where the code was very intense, in terms of logic.
  In terms of the code he handled, he contributed a decent  amount to this project.

Contributions:

Rohan
- In terms of coding, I contributed to creating the games within play_bs.cpp, and the testing of play_bs.cpp. During our group meetings, I helped contribute ideas regarding logic for the header files
  for ttt and bs, as well as tictactoe.cpp and battleship.cpp,  but contributed primarily through coding for play_bs.cpp.

Saksham
- We all contributed equally in making an elaborate mind map for the homework. I contributed to creating the games (test cases) within play_ttt.cpp.
  I contributed regarding the logic for tictactoe.cpp and battleship.cpp (in which a significant amount of coding was done by Aidan).



#ifndef BATTLE_SHIP_LITE_H
#define BATTLE_SHIP_LITE_H

/*
 *Aidan Reilly
 *Saksham Bhandari
 *Rohan Puppaa
 */

#include "game.h"
#include <array>
#include <cstdlib>
#include <iostream>

class BattleshipGame: public Game {
 public:
  // Constructs BattleshipGame
  BattleshipGame(std::array<Coord, 5> player1_ships, std::array<Coord, 5> player2_ships);

  // A move is valid in battleship if the spot contains
  // nothing (0), or an enemy ship (2), and invalid if the spot constains
  // a white peg (1) or a red peg/sunken enemy ship (3) 
  virtual bool is_valid_move(Coord spot, Board board) const;

  // Attempt to attack a square of the opposing player's board and return GameResult.
  virtual GameResult attack_square(Coord spot);

  // Return the result/status of the game
  virtual GameResult check_game_result (int curr_player);

};

#endif
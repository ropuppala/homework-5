#include "tictactoe.h"

/*
 *Aidan Reilly
 *Saksham Bhandari
 *Rohan Puppaa
 */

using namespace std;

// If the given square is valid to attack, places an X or an O there
// depending on whose turn it is. Returns GameResult.
GameResult TicTacToeGame::attack_square(Coord spot) {
  Board& only_board = get_board(0);
  if (is_valid_move(spot, only_board)){
    if (whose_turn() == 1){
      only_board.mark_spot(spot, TTT_O);
    }
    else {
      only_board.mark_spot(spot, TTT_X);
    }
    GameResult result = check_game_result(whose_turn());
    update_turn();
    return result;
  }
  else {
    return RESULT_INVALID;
  }
}

// Returns the result of the game 
GameResult TicTacToeGame::check_game_result(int curr_player){
  Board& only_board = get_board(0);
 int open_spaces = 0;

 for (int i = 0; i < 3; i++){
  // Check rows
   if (((only_board.at_spot(make_pair(i,0))) == curr_player)
       && ((only_board.at_spot(make_pair(i,1))) == curr_player)
       && ((only_board.at_spot(make_pair(i,2))) == curr_player)){
    return (GameResult) (curr_player + 2);
  }

  // Check columns
   else if (((only_board.at_spot(make_pair(0,i))) == curr_player)
	    && ((only_board.at_spot(make_pair(1,i))) == curr_player)
	    && ((only_board.at_spot(make_pair(2,i))) == curr_player)){
    return (GameResult) (curr_player + 2);
  }

  // Count open spaces
  for (int j = 0; j < 3; j++){
    if((only_board.at_spot(make_pair(i,j))) == 0){
      open_spaces++;
    }
  }

 }

 // Check diagonal starting in top left corner
 if (((only_board.at_spot(make_pair(0,0))) == curr_player)
     && ((only_board.at_spot(make_pair(1,1))) == curr_player)
     && ((only_board.at_spot(make_pair(2,2))) == curr_player)){
   return (GameResult) (curr_player + 2);
 }

 // Check diagonal starting at top right corner
 else if (((only_board.at_spot(make_pair(0,2))) == curr_player)
	  && ((only_board.at_spot(make_pair(1,1))) == curr_player)
	  && ((only_board.at_spot(make_pair(2,0))) == curr_player)){
   return (GameResult) (curr_player + 2);
 }

 // Check open spaces
 else if (open_spaces == 0){
   return RESULT_STALEMATE;
 }

 else {
   return RESULT_KEEP_PLAYING;
 }  

}  
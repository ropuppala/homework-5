#include "battleship.h"

/*
 *Aidan Reilly
 *Saksham Bhandari
 *Rohan Puppaa
 */

using namespace std;

// Constructs a BattleshipGame
BattleshipGame::BattleshipGame(std::array<Coord, 5> player1_ships, std::array<Coord, 5> player2_ships): Game(10, 2) {
    Board& board1 = get_board(0);
    Board& board2 = get_board(1);
    for (int i = 0; i < 5; i++){
      if (player1_ships.at(i).first >= 0 && player1_ships.at(i).first < 10
	  && player1_ships.at(i).second >= 0 && player1_ships.at(i).second < 10
	  && player2_ships.at(i).first >= 0 && player2_ships.at(i).first < 10
	  && player2_ships.at(i).second >= 0 && player2_ships.at(i).second < 10){
      board1.mark_spot(player1_ships.at(i), BS_FLOATING_SHIP);
      board2.mark_spot(player2_ships.at(i), BS_FLOATING_SHIP);
      }
      else{
	std::cout << "ERROR, SHIPS OUT OF BOUNDS" << std::endl;
	exit(EXIT_FAILURE);
      }
    }
  }


// A move is valid in battleship if the spot contains
// nothing (0), or an enemy ship (2), and invalid if the spot constains
// a white peg (1) or a red peg/sunken enemy ship (3) 
bool BattleshipGame::is_valid_move(Coord spot, Board board) const{
  if (spot.first >= 0 && spot.first < board.get_size()
      && spot.second >= 0 && spot.second < board.get_size()){
    if (board.at_spot(spot) == 0 || board.at_spot(spot) == BS_FLOATING_SHIP){
      return true;
    }
    else{
      return false;
    }
  }
  else{
    return false;
  }
}

// Attempt to attack a square of the opposing player's board and return GameResult.
GameResult BattleshipGame::attack_square(Coord spot){
  int attacked_board_num = 1;
  if (whose_turn() == 2){
    attacked_board_num = 0;
  }
  Board& attacked_board = get_board(attacked_board_num);
  if (is_valid_move(spot, attacked_board)){
    int current_at_spot = attacked_board.at_spot(spot);
    attacked_board.mark_spot(spot, (current_at_spot + 1));
    GameResult result = check_game_result(whose_turn());
    update_turn();
    return result;
  }
  else {
    return RESULT_INVALID;
  }
}

// Return the result/status of the game
GameResult BattleshipGame::check_game_result (int curr_player){
  int attacked_board_num = 1;
  if (curr_player == 2){
    attacked_board_num = 0;
  }
  Board& attacked_board = get_board(attacked_board_num);
  int num_sunken = 0;
  for (int i = 0; i < 10; i ++){
    for (int j = 0; j < 10; j++){
      if (attacked_board.at_spot(make_pair(i,j)) == BS_RED){
	num_sunken++;
      }
    }
  }
  if (num_sunken == 5){
    return (GameResult) (curr_player + 2);
  }

  else{
    return RESULT_KEEP_PLAYING;
  }
  
}
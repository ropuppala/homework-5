#ifndef GAME_H
#define GAME_H

/*
 * Use this file as a *starting point*.  You may add more classes and other
 * definitions here.
 *
 * We have suggested a couple ideas (Board and Game) for base classes you can
 * use in your design.
 *
 * Implement derived types for the games in "battleship.h" and "tictactoe.h"
 *
 * You must use "enum GameResult" and "Coord" as defined here, and you must
 * implement derived types called BattleshipGame and TicTacToeGame, each with
 * a public attack_square member function, as you can see from reading
 * play_bs.cpp and play_ttt.cpp.
 */

/*
 *Aidan Reilly
 *Saksham Bhandari
 *Rohan Puppaa
 */

#include <utility>
#include <vector>

enum GameResult {
	RESULT_KEEP_PLAYING, // turn was valid and game is not over yet
	RESULT_INVALID,      // turn was invalid; e.g. attacked square
	                     // was attacked previously
	RESULT_STALEMATE,    // game over, neither player wins
	RESULT_PLAYER1_WINS, // game over, player 1 wins
	RESULT_PLAYER2_WINS  // game over, player 2 wins
};

enum TTT_Spot {
  TTT_O = 1, // O's are 1's on the board
  TTT_X // X's are 2's on the board
};

enum BS_Spot {
  BS_WHITE = 1, // Guessed white pegs are 1's on the board
  BS_FLOATING_SHIP, // Floating opponent ships are 2's on the board
  BS_RED // Sunken opponent ships are 3's on the board
};

typedef std::pair<int, int> Coord;

class Board {
 public:
  // Constructs a Board
  Board(int size);

  // Marks a given spot on the board with given int
  // if the spot is on the board, otherwise does nothing.
  void mark_spot(Coord spot, int mark);

  // Returns the int at given spot on board.
  int at_spot(Coord spot) const{
    return board.at(spot.first).at(spot.second);
  }

  // returns size of board
  int get_size() const{
    return board_size;
  }
  
 private:
  int board_size; //size of the board
  std::vector<std::vector<int> > board;
};

class Game {
 public:
  // Constructs a Game
  Game(int size, int num_boards);
  
  // Return whose turn it is to move
  int whose_turn() const{
    return player;
  }

  // Updates whose turn it is
  void update_turn();

  // checks if a player is allowed to attack that spot
  virtual bool is_valid_move(Coord spot, Board board) const;

  // Checks if a player wins after making a move.
  virtual GameResult check_game_result (int curr_player) = 0;
  
  // Attempts to attack a a spot on the board.
  // Returns the state of the game as GameResult.
  virtual GameResult attack_square(Coord spot) = 0;

  // Returns the board at the given index the the game's
  // vector of boards.
  Board& get_board(int board_num){
    return b[board_num];
  }
  
 private:
  int player; // whose turn it is to move
  std::vector<Board> b; // vector of boards being played with
};

#endif
#include "battleship.h"
#include <iostream>
#include <cassert>

using namespace std;

void game1()
{
	array<Coord, 5> player1_ships = {{
		make_pair(2, 2),
		make_pair(4, 4),
		make_pair(5, 5),
		make_pair(6, 6),
		make_pair(8, 8)
	}};
	array<Coord, 5> player2_ships = {{
		make_pair(2, 2),
		make_pair(4, 4),
		make_pair(5, 5),
		make_pair(6, 6),
		make_pair(8, 8)
	}};
	BattleshipGame bsg(player1_ships, player2_ships);
	
	GameResult result = bsg.attack_square(make_pair(2, 2)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	
	result = bsg.attack_square(make_pair(2, 2)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);
	
	//RED PEG ALREADY EXITS IN SPOT(HIT)
	result = bsg.attack_square(make_pair(2, 2)); // P1 INVALID MOVE
	assert(result == RESULT_INVALID);
	 
	// note: an invalid move doesn't count as a turn; still player 1's turn
	
	result = bsg.attack_square(make_pair(4, 4)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	
	result = bsg.attack_square(make_pair(4, 4)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(5, 5)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	
	result = bsg.attack_square(make_pair(5, 5)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(6, 6)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);
	
	result = bsg.attack_square(make_pair(6, 6)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(8, 8)); // P1 HIT
	assert(result == RESULT_PLAYER1_WINS);

	cout << "PASSED game 1" << endl;
}

void game2()
{
        array<Coord, 5> player1_ships = {{
                make_pair(1, 1),
                make_pair(3, 3),
                make_pair(7, 7),
                make_pair(8, 7),
                make_pair(1, 2)
        }};
        array<Coord, 5> player2_ships = {{
                make_pair(1, 3),
                make_pair(2, 1),
                make_pair(1, 4),
                make_pair(2, 3),
                make_pair(3, 8)
        }};
        BattleshipGame bsg(player1_ships, player2_ships);
        
        GameResult result = bsg.attack_square(make_pair(2, 2)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(1, 3)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);
        
        //WHITE PEG ALREADY EXITS IN SPOT(MISS)
        result = bsg.attack_square(make_pair(2, 2)); // P1 INVALID MOVE
        assert(result == RESULT_INVALID);
        
        // note: an invalid move doesn't count as a turn; still player 1's turn
        
        result = bsg.attack_square(make_pair(1, 4)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
	//NOT IN BOUNDS
        result = bsg.attack_square(make_pair(10, 4)); // P2 ERROR  
        assert(result == RESULT_INVALID);
	
	//invalid will result in P2's turn again

        result = bsg.attack_square(make_pair(7, 7)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(3, 8)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(1, 1)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(6, 6)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(8, 7)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);
	
	result = bsg.attack_square(make_pair(2, 3)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(3, 3)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(4, 4)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(1, 2)); // P2 HIT
        assert(result == RESULT_PLAYER2_WINS);
        cout << "PASSED game 2" << endl;
}
void game3()
{
        array<Coord, 5> player1_ships = {{
                make_pair(0, 2),
                make_pair(2, 4),
                make_pair(5, 7),
                make_pair(7, 6),
                make_pair(8, 3)
        }};
        array<Coord, 5> player2_ships = {{
                make_pair(3, 2),
                make_pair(1, 8),
                make_pair(6, 5),
                make_pair(0, 6),
                make_pair(5, 8)
        }};
        BattleshipGame bsg(player1_ships, player2_ships);

	//NOT IN BOUNDS        
        GameResult result = bsg.attack_square(make_pair(2, 11)); // P1 INVALID MOVE
        assert(result == RESULT_INVALID);

        result = bsg.attack_square(make_pair(3, 2)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(0, 2)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(1, 8)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
	//RED PEG ALREADY EXITS IN SPOT(HIT)
        result = bsg.attack_square(make_pair(0, 2)); // P2 INVALID MOVE
        assert(result == RESULT_INVALID);
        
        result = bsg.attack_square(make_pair(4, 4)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(0, 6)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(5, 7)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(5, 8)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(0, 0)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(6, 5)); // P1 HIT
        assert(result == RESULT_PLAYER1_WINS);

        cout << "PASSED game 3" << endl;
}

void game4()
{
        array<Coord, 5> player1_ships = {{
                make_pair(1, 7),
                make_pair(4, 3),
                make_pair(7, 5),
                make_pair(6, 3),
                make_pair(6, 8)
        }};
        array<Coord, 5> player2_ships = {{
                make_pair(2, 5),
                make_pair(2, 7),
                make_pair(0, 5),
                make_pair(3, 6),
                make_pair(5, 0)
        }};
        BattleshipGame bsg(player1_ships, player2_ships);

	//NOT IN BOUNDS
        GameResult result = bsg.attack_square(make_pair(10, 10)); // P1 INVALID MOVE
        assert(result == RESULT_INVALID);
        
        result = bsg.attack_square(make_pair(2, 5)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
       
        result = bsg.attack_square(make_pair(1, 7)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(0, 5)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(4, 3)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(5, 0)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(6, 3)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(6, 2)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(6, 8)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(3, 6)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(7, 5)); // P2 HIT
        assert(result == RESULT_PLAYER2_WINS);

        cout << "PASSED game 4" << endl;
}

void game5()
{
        array<Coord, 5> player1_ships = {{
                make_pair(0, 0),
                make_pair(4, 2),
                make_pair(5, 6),
                make_pair(6, 7),
                make_pair(8, 5)
        }};
        array<Coord, 5> player2_ships = {{
                make_pair(7, 2),
                make_pair(4, 0),
                make_pair(5, 3),
                make_pair(6, 0),
                make_pair(8, 4)
        }};
        BattleshipGame bsg(player1_ships, player2_ships);
        
        GameResult result = bsg.attack_square(make_pair(7, 2)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(2, 4)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(5, 3)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(7, 4)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(8, 4)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(3, 5)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(4, 0)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(8, 6)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(6, 0)); // P1 HIT
        assert(result == RESULT_PLAYER1_WINS);

        cout << "PASSED game 5" << endl;
}

void game6()
{
        array<Coord, 5> player1_ships = {{
                make_pair(2, 0),
                make_pair(4, 8),
                make_pair(5, 2),
                make_pair(6, 2),
                make_pair(8, 0)
        }};
        array<Coord, 5> player2_ships = {{
                make_pair(1, 5),
                make_pair(4, 6),
                make_pair(1, 6),
                make_pair(7, 3),
                make_pair(8, 1)
        }};
        BattleshipGame bsg(player1_ships, player2_ships);
        
        GameResult result = bsg.attack_square(make_pair(1, 6)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(2, 1)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(7, 3)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(2, 0)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(7, 3)); // P1 INVALID MOVE
        assert(result == RESULT_INVALID);

        result = bsg.attack_square(make_pair(8, 6)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(6, 2)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

	//WHITE PEG ALREADY EXITS IN SPOT(MISS)
        result = bsg.attack_square(make_pair(8, 6)); // P1 INVALID MOVE
        assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(1, 1)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(5, 2)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(8, 1)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(4, 8)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(5, 6)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(8, 0)); // P2 HIT
        assert(result == RESULT_PLAYER2_WINS);

        cout << "PASSED game 6" << endl;
}

void game7()
{
        array<Coord, 5> player1_ships = {{
                make_pair(0, 4),
                make_pair(1, 0),
                make_pair(7, 1),
                make_pair(3, 7),
                make_pair(2, 8)
        }};
        array<Coord, 5> player2_ships = {{
                make_pair(0, 7),
                make_pair(4, 1),
                make_pair(5, 4),
                make_pair(0, 8),
                make_pair(8, 2)
        }};
        BattleshipGame bsg(player1_ships, player2_ships);
        
        GameResult result = bsg.attack_square(make_pair(0, 7)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(0, 4)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(4, 1)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(1, 0)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(5, 4)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(3, 7)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(0, 8)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(2, 8)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(8, 2)); // P1 HIT
        assert(result == RESULT_PLAYER1_WINS);

        cout << "PASSED game 7" << endl;
}

void game8()
{
        array<Coord, 5> player1_ships = {{
                make_pair(5, 1),
                make_pair(7, 0),
                make_pair(8, 6),
                make_pair(3, 0),
                make_pair(2, 6)
        }};
        array<Coord, 5> player2_ships = {{
                make_pair(6, 1),
                make_pair(7, 4),
                make_pair(7, 8),
                make_pair(3, 4),
                make_pair(4, 7)
        }};
        BattleshipGame bsg(player1_ships, player2_ships);
        
        GameResult result = bsg.attack_square(make_pair(4, 7)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
	//NOT IN BOUNDS
        result = bsg.attack_square(make_pair(10, 10)); // P2 INVALID MOVE
        assert(result == RESULT_INVALID);
	
	//NOT IN BOUNDs
	result = bsg.attack_square(make_pair(10, 3)); // P2 INVALID MOVE
        assert(result == RESULT_INVALID);

        result = bsg.attack_square(make_pair(2, 8)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(4, 7)); // P1 INVALID MOVE
        assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(7, 2)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(8, 6)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(1, 3)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(3, 3)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(3, 4)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(6, 7)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(5, 8)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(7, 0)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

	//WHITE PEG ALREADY EXITS IN SPOT(MISS)
	result = bsg.attack_square(make_pair(5, 8)); // P1 INVALID MOVE
        assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(7, 8)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(6, 5)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(2, 8)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(3, 0)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(6, 1)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(5, 1)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(7, 4)); // P1 HIT
        assert(result == RESULT_PLAYER1_WINS);

	cout << "PASSED game 8" << endl;
}

void game9()
{
        array<Coord, 5> player1_ships = {{
                make_pair(0, 1),
                make_pair(1, 0),
                make_pair(3, 1),
                make_pair(3, 5),
                make_pair(2, 6)
        }};
        array<Coord, 5> player2_ships = {{
                make_pair(0, 3),
                make_pair(7, 1),
                make_pair(5, 4),
                make_pair(4, 5),
                make_pair(6, 4)
        }};
        BattleshipGame bsg(player1_ships, player2_ships);
        
        GameResult result = bsg.attack_square(make_pair(0, 3)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
	//NOT IN BOUNDS
       	result = bsg.attack_square(make_pair(-14, -4)); // P2 INVALID MOVE
        assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(1, 0)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
       	result = bsg.attack_square(make_pair(7, 1)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
       	result = bsg.attack_square(make_pair(0, 0)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(5, 4)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
       	result = bsg.attack_square(make_pair(3, 5)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(6, 4)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(3, 1)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(4, 5)); // P1 HIT
        assert(result == RESULT_PLAYER1_WINS);

	cout << "PASSED game 9" << endl;
}

void game10()
{
        array<Coord, 5> player1_ships = {{
                make_pair(0, 9),
                make_pair(3, 9),
                make_pair(5, 9),
                make_pair(9, 1),
                make_pair(9, 3)
        }};
        array<Coord, 5> player2_ships = {{
                make_pair(1, 9),
                make_pair(7, 9),
                make_pair(9, 4),
                make_pair(9, 5),
                make_pair(9, 9)
        }};
        BattleshipGame bsg(player1_ships, player2_ships);
        
        GameResult result = bsg.attack_square(make_pair(1, 9)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        //NOT IN BOUNDS
        result = bsg.attack_square(make_pair(-14, 5)); // P2 INVALID MOVE
        assert(result == RESULT_INVALID);

        result = bsg.attack_square(make_pair(0, 9)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(7, 9)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(0, 3)); // P2 MISS
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(9, 4)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(3, 9)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(9, 5)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(5, 9)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(9, 9)); // P1 HIT
        assert(result == RESULT_PLAYER1_WINS);

        cout << "PASSED game 10" << endl;
}

void game11()
{
        array<Coord, 5> player1_ships = {{
                make_pair(9, 0),
                make_pair(1, 0),
                make_pair(9, 2),
                make_pair(9, 6),
                make_pair(2, 9)
        }};
        array<Coord, 5> player2_ships = {{
                make_pair(9, 7),
                make_pair(8, 9),
                make_pair(9, 8),
                make_pair(4, 9),
                make_pair(6, 9)
        }};
        BattleshipGame bsg(player1_ships, player2_ships);
        
        GameResult result = bsg.attack_square(make_pair(9, 7)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(9, 0)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(1, 6)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(9, 6)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(6, 9)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(1, 0)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);
        
        result = bsg.attack_square(make_pair(3, 5)); // P1 MISS
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(2, 9)); // P2 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(4, 9)); // P1 HIT
        assert(result == RESULT_KEEP_PLAYING);

        result = bsg.attack_square(make_pair(9, 2)); // P2 HIT
        assert(result == RESULT_PLAYER2_WINS);

        cout << "PASSED game 11" << endl;
}

void game12()
{
  // Player 1's first ship is out of bounds
        array<Coord, 5> player1_ships = {{
                make_pair(10, 0),
                make_pair(1, 0),
                make_pair(9, 2),
                make_pair(9, 6),
                make_pair(2, 9)
        }};
        array<Coord, 5> player2_ships = {{
                make_pair(9, 7),
                make_pair(8, 9),
                make_pair(9, 8),
                make_pair(4, 9),
                make_pair(6, 9)
        }};
        BattleshipGame bsg(player1_ships, player2_ships); // At this point the program will print an error message and exit
}

int main(void)
{
	game1();
	//Add more games as test
	//Three Exceptions/Invalid that are checked: NOT IN BOUNDS, WHITE PEG ALREADY EXISTS IN SPOT(MISS), RED PEG ALREADY EXITS IN SPOT(HIT)
	game2();
	game3();
	game4();
	game5();
	game6();
	game7();
	//Longest game with multiple exceptions, even two in a row, simply to check if the code can run smoothly with a longer game
	game8();
	//Tests point in negative bounds
	game9();
	game10();
	game11();
	game12(); // won't pass but instead will print an error message and then exit
	return 0;
}

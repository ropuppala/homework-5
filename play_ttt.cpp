#include "tictactoe.h"
#include <iostream>
#include <cassert>

using namespace std; 

//game 1 - tests for tie and invalid move (user over-writes on the other players move)
void game1()
{
	TicTacToeGame tttg;
	
	// Initial state:
	
	// - - -
	// - - -
	// - - -
	
	GameResult result = tttg.attack_square(make_pair(1, 1));
	assert(result == RESULT_KEEP_PLAYING);
	
	// State after previous turn:
	
	// - - -
	// - O -
	// - - -
	
	result = tttg.attack_square(make_pair(0, 0));
	assert(result == RESULT_KEEP_PLAYING);
	
	// X - -
	// - O -
	// - - -
	
	// oops, already an X on this square
	result = tttg.attack_square(make_pair(0, 0));
	assert(result == RESULT_INVALID);

	// note: an invalid move doesn't count as a turn; still O's turn

	result = tttg.attack_square(make_pair(0, 1));
	assert(result == RESULT_KEEP_PLAYING);
	
	// X O -
	// - O -
	// - - -
	
	result = tttg.attack_square(make_pair(2, 1));
	assert(result == RESULT_KEEP_PLAYING);
	
	// X O -
	// - O -
	// - X -
	
	result = tttg.attack_square(make_pair(1, 0));
	assert(result == RESULT_KEEP_PLAYING);
	
	// X O -
	// O O -
	// - X -
	
	result = tttg.attack_square(make_pair(1, 2));
	assert(result == RESULT_KEEP_PLAYING);
	
	// X O -
	// O O X
	// - X -
	
	result = tttg.attack_square(make_pair(0, 2));
	assert(result == RESULT_KEEP_PLAYING);
	
	// X O O
	// O O X
	// - X -
	
	result = tttg.attack_square(make_pair(2, 0));
	assert(result == RESULT_KEEP_PLAYING);
	
	// X O O
	// O O X
	// X X -
	
	result = tttg.attack_square(make_pair(2, 2));
	assert(result == RESULT_STALEMATE);
	
	// X O O
	// O O X
	// X X O
	
	cout << "PASSED game 1" << endl;
}

//game 2 - tests for player 1 win (top right diagonal) and invalid move (player over-writes on his own move)
void game2()
{
        TicTacToeGame tttg;

        // Initial state:

        // - - -
        // - - -
        // - - -

        GameResult result = tttg.attack_square(make_pair(1, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // State after previous turn:

        // - - -
        // - O -
        // - - -

        result = tttg.attack_square(make_pair(0, 0));
        assert(result == RESULT_KEEP_PLAYING);

        // X - -
        // - O -
        // - - -

        // oops, player trying to over-write his/her own move
        result = tttg.attack_square(make_pair(1, 1));
        assert(result == RESULT_INVALID);

        // note: an invalid move doesn't count as a turn; still O's turn

        result = tttg.attack_square(make_pair(0, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // X - O
        // - O -
        // - - -

        result = tttg.attack_square(make_pair(2, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // X - O
        // - O -
        // - X -

        result = tttg.attack_square(make_pair(2, 0));
        assert(result == RESULT_PLAYER1_WINS);

        // X - O
        // - O -
        // O X -

        cout << "PASSED game 2" << endl;
}

//game 3 - tests for player 2 win (column 1 win)  and invalid move (player one tries to move outside the board at negative coordinates)
void game3()
{
        TicTacToeGame tttg;

        // Initial state:

        // - - -
        // - - -
        // - - -

        GameResult result = tttg.attack_square(make_pair(1, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // State after previous turn:

        // - - -
        // - O -
        // - - -

        result = tttg.attack_square(make_pair(0, 0));
        assert(result == RESULT_KEEP_PLAYING);

        // X - -
        // - O -
        // - - -

	// oops, player trying to make a move outside the board
	result = tttg.attack_square(make_pair(-1,-1));
	assert(result == RESULT_INVALID);

	// X - -
        // - O -
        // - - -

	// note: an invalid move doesn't count as a turn; still O's turn
	
        result = tttg.attack_square(make_pair(0, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // X - O
        // - O -
        // - - -

        result = tttg.attack_square(make_pair(2, 0));
        assert(result == RESULT_KEEP_PLAYING);

        // X - O
        // - O -
        // X - -

        result = tttg.attack_square(make_pair(2, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // X - O
        // - O -
        // X - O

        result = tttg.attack_square(make_pair(1, 0));
        assert(result == RESULT_PLAYER2_WINS);

        // X - O
        // X O -
        // X - O


        cout << "PASSED game 3" << endl;
}

//game 4 - tests for player 1 win (top left diagonal) and invalid move (player one tries to move outside the board)
void game4()
{
        TicTacToeGame tttg;

        // Initial state:

        // - - -
        // - - -
        // - - -

	GameResult result = tttg.attack_square(make_pair(1, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // State after previous turn:
        // - - -
        // - O -
        // - - -

	result = tttg.attack_square(make_pair(0, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // - - X
        // - O -
        // - - -

        // oops, player trying to make a move outside the board
        result = tttg.attack_square(make_pair(3, 3));
        assert(result == RESULT_INVALID);

        // note: an invalid move doesn't count as a turn; still O's turn

        result = tttg.attack_square(make_pair(0, 0));
        assert(result == RESULT_KEEP_PLAYING);

        // O - X
        // - O -
        // - - -

        result = tttg.attack_square(make_pair(1, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // O - X
        // - O X
        // - - -

        result = tttg.attack_square(make_pair(2, 2));
        assert(result == RESULT_PLAYER1_WINS);

        // O - X
        // - O X
        // - - O


        cout << "PASSED game 4" << endl;
}

//game 5 - tests for player 2 win (column 3 win)
void game5()
{
        TicTacToeGame tttg;

        // Initial state:

        // - - -
        // - - -
        // - - -

        GameResult result = tttg.attack_square(make_pair(1, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // State after previous turn:
        // - - -
        // - O -
        // - - -

        result = tttg.attack_square(make_pair(0, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // - - X
        // - O -
        // - - -

        result = tttg.attack_square(make_pair(0, 0));
        assert(result == RESULT_KEEP_PLAYING);

        // O - X
        // - O -
        // - - -

        result = tttg.attack_square(make_pair(1, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // O - X
        // - O X
        // - - -

        result = tttg.attack_square(make_pair(2, 0));
        assert(result == RESULT_KEEP_PLAYING);

        // O - X
        // - O X
        // O - -

        result = tttg.attack_square(make_pair(2,2));
        assert(result == RESULT_PLAYER2_WINS);

        // O - X
        // - O X
        // O - X

        cout << "PASSED game 5" << endl;
}

//game 6 - tests for player 1 win (row 1 win)
void game6()
{
        TicTacToeGame tttg;

        // Initial state:

        // - - -
        // - - -
        // - - -

        GameResult result = tttg.attack_square(make_pair(0, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // State after previous turn:
        // - O -
        // - - -
        // - - -      

        result = tttg.attack_square(make_pair(2, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // - O -
        // - - -
        // - - X

        result = tttg.attack_square(make_pair(0, 0));
        assert(result == RESULT_KEEP_PLAYING);

        // O O -
        // - - -
        // - - X

        result = tttg.attack_square(make_pair(1, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // O O -
        // - - X
        // - - X

        result = tttg.attack_square(make_pair(0, 2));
        assert(result == RESULT_PLAYER1_WINS);

        // O O O
        // - - X
        // - - X


        cout << "PASSED game 6" << endl;
}

//game 7 - tests for player 2 win (row 3 win)
void game7()
{
        TicTacToeGame tttg;

        // Initial state:

        // - - -
        // - - -
        // - - -

        GameResult result = tttg.attack_square(make_pair(0, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // State after previous turn:
        // - O -
        // - - -
        // - - -

        result = tttg.attack_square(make_pair(2, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // - O -
        // - - -
        // - - X

        result = tttg.attack_square(make_pair(0, 0));
        assert(result == RESULT_KEEP_PLAYING);

        // O O -
        // - - -
        // - - X

        result = tttg.attack_square(make_pair(2, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // O O -
        // - - -
        // - X X

        result = tttg.attack_square(make_pair(1, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // O O -
        // - O -
        // - X X

        result = tttg.attack_square(make_pair(2, 0));
        assert(result == RESULT_PLAYER2_WINS);

        // O O -
        // - O -
        // X X X


        cout << "PASSED game 7" << endl;
}

//game 8 - tests for player 1 win (row 2 win)
void game8()
{
        TicTacToeGame tttg;

        // Initial state:

        // - - -
        // - - -
        // - - -

        GameResult result = tttg.attack_square(make_pair(1, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // State after previous turn:
        // - - -
        // - O -
        // - - -

        result = tttg.attack_square(make_pair(2, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // - - -
        // - O -
        // - - X

        result = tttg.attack_square(make_pair(1, 0));
        assert(result == RESULT_KEEP_PLAYING);

        // - - -
        // O O -
        // - - X

        result = tttg.attack_square(make_pair(2, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // - - -
        // O O -
        // - X X

        result = tttg.attack_square(make_pair(1, 2));
        assert(result == RESULT_PLAYER1_WINS);

        // - - -
        // O O O
        // - X X

        cout << "PASSED game 8" << endl;
}

//game 9 - tests for player 2 win (column 2 win)
void game9()
{
        TicTacToeGame tttg;

        // Initial state:

        // - - -
        // - - -
        // - - -

        GameResult result = tttg.attack_square(make_pair(2, 2));
        assert(result == RESULT_KEEP_PLAYING);

        // State after previous turn:
        // - - -
        // - - -
        // - - O

        result = tttg.attack_square(make_pair(1, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // - - -
        // - X -
        // - - O

        result = tttg.attack_square(make_pair(2, 0));
        assert(result == RESULT_KEEP_PLAYING);

        // - - -
        // - X -
        // O - O

        result = tttg.attack_square(make_pair(0, 1));
        assert(result == RESULT_KEEP_PLAYING);

        // - X -
        // - X -
        // O - O

        result = tttg.attack_square(make_pair(0, 0));
        assert(result == RESULT_KEEP_PLAYING);

        // O X -
        // - X -
        // O - O

        result = tttg.attack_square(make_pair(2, 1));
        assert(result == RESULT_PLAYER2_WINS);

        // O X -
        // - X -
        // O X O

        cout << "PASSED game 9" << endl;
}

int main(void)
{
	game1();
	game2();
	game3();
	game4();
	game5();
	game6();
	game7();
	game8();
	game9();

	return 0;
}

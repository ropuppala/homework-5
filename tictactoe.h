#ifndef TIC_TAC_TOE_H
#define TIC_TAC_TOE_H

/*
 *Aidan Reilly
 *Saksham Bhandari
 *Rohan Puppaa
 */

#include "game.h"

class TicTacToeGame: public Game {
 public:
 TicTacToeGame(): Game(3, 1) {}

  // If the given square is valid to attack, places an X or an O there
  // depending on whose turn it is. Returns GameResult.
  virtual GameResult attack_square(Coord spot);

  // Returns the result of the game
  virtual GameResult check_game_result(int curr_player);
   
};

#endif